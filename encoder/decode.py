#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import statistics
import sys
import os.path
from collections import Counter
from collections import OrderedDict
import oligoutil
import settings

if len(sys.argv) != 5:
    print('./decode <fileid map file> <data freq file> <dict freq file> <oligofile>\n')
    sys.exit(1)

def parse_oligos(oligo_file):
    data_strings = []
    dict_strings = []
    lena_strings = []
    mezzanine_strings = []
    with open(oligo_file, 'r') as f:
        for line in f:
            #remove priers and sense NTs
            oligo_str = line.rstrip()
            assert oligo_str[:settings.G_5PRIMER_LEN] == settings.G_5_PRIMER
            assert oligo_str[-settings.G_3PRIMER_LEN:] == settings.G_3_PRIMER
            oligo_str = oligo_str[settings.G_5PRIMER_LEN + 1 :
                    -(settings.G_3PRIMER_LEN + 1)]
            oligoutil.dprint(oligo_str)

            #decode metadata (direction trit, fileid trits, parity trit)
            metadata_len = 1 + settings.G_FILEID_CHARS + 1
            oligo_metadata = oligo_str[40:40 + metadata_len]
            oligoutil.dprint(oligo_metadata)
            encoded_metadata = oligoutil.decode_oligo(oligo_metadata)
            oligoutil.dprint(encoded_metadata)

            #get and remove file id
            fileid_str = encoded_metadata[1:-1]
            oligoutil.dprint(fileid_str)
            assert len(fileid_str) == settings.G_FILEID_CHARS
            fileid = oligoutil.todecimal(fileid_str) 
            if (fileid == 9):
                mezzanine_strings.append(line)
                continue

            if (fileid == 10):
                lena_strings.append(line)
                continue

            #decode data
            oligo_str = oligo_str[0:40] + oligo_str[40 + metadata_len: ]
            oligoutil.dprint(oligo_str)
            if (encoded_metadata[0] == '1'):
                oligo_str = oligoutil.rcomplement(oligo_str)                
                oligoutil.dprint("reverse complimenting reversed oligo:" + oligo_str)
            encoded_data = oligoutil.decode_oligo(oligo_str)
            oligoutil.dprint(encoded_data)

            # check and remove parity
            parity_str = encoded_metadata[-1:]
            oligoutil.dprint(parity_str)
            encoded_metadata = encoded_metadata[:-1]
            if oligoutil.compute_parity(encoded_data + encoded_metadata) != parity_str:
                print('Parity verification failed.')
                print('oligo:' + line.rstrip())
                print('decode data:' + encoded_data)
                print('decode metadata:' + encoded_metadata)
                print('computed parity: ' +
                        str(oligoutil.compute_parity(encoded_data + encoded_metadata)))

                print('stored parity: ' + parity_str)
                sys.exit(1)
  
            if 'dict' in fileid_map[fileid]:
                dict_strings.append(encoded_data + fileid_str)
            else:
                data_strings.append(encoded_data + fileid_str)

    return data_strings, dict_strings, lena_strings, mezzanine_strings

def decode_data(data_strings, ternary_huffman):
    encoded_map = {}

    for encoded_str in data_strings:
        fileid_str = encoded_str[-settings.G_FILEID_CHARS:]
        encoded_str = encoded_str[: -settings.G_FILEID_CHARS]
        fileid = oligoutil.todecimal(fileid_str) 

	#get base3 length
        base3_str = encoded_str[-settings.G_NBASE3_LEN_CHARS:]
        length = oligoutil.todecimal(base3_str)
        encoded_str = encoded_str[: -settings.G_NBASE3_LEN_CHARS]

	#remove padding
        encoded_str = encoded_str[0 : length]
	
	#huffman decode 
        oligoutil.dprint('Huffman string: ' + encoded_str)
        decoded_str = ''.join(ternary_huffman.decode(encoded_str))
        oligoutil.dprint('Decoded str: ' + decoded_str)
        if fileid not in encoded_map:
            encoded_map[fileid] = []

        encoded_map[fileid].append(decoded_str)

    return encoded_map

def decode_dict(dict_strings, ternary_huffman):
    segments = {}

    for encoded_str in dict_strings:
        #get fileid
        fileid_str = encoded_str[-settings.G_FILEID_CHARS:]
        fileid = oligoutil.todecimal(fileid_str) 
        encoded_str = encoded_str[: -settings.G_FILEID_CHARS]

	#get base3 length
        base3_str = encoded_str[-settings.G_NBASE3_LEN_CHARS:]
        length = oligoutil.todecimal(base3_str)
        encoded_str = encoded_str[:-settings.G_NBASE3_LEN_CHARS]

        # get index
        index_str = encoded_str[-settings.G_MAX_INDEX_LEN:]
        chunk_idx = oligoutil.todecimal(index_str)
        encoded_str = encoded_str[:-settings.G_MAX_INDEX_LEN]

	#remove padding
        encoded_str = encoded_str[0 : length]
	
        #add string at index
        if chunk_idx in segments:
            assert segments[chunk_idx] == encoded_str

        segments[chunk_idx] = encoded_str

    #now we have all segments. merge it all and decode
    huffman_str = ''.join(segments[x] for x in sorted(segments))
    oligoutil.dprint('Length of encoded str: ' + str(len(huffman_str)))

    decoded_bytes = ternary_huffman.decode(huffman_str)

    return bytearray(int(c) for c in decoded_bytes)

data_freq_file = ''
dict_freq_file = ''
fileid_map_file = ''
oligo_file = ''
for arg in sys.argv[1:]:
    if 'dict.freqs' in arg:
        dict_freq_file = arg
    elif 'data.freqs' in arg:
        data_freq_file = arg
    elif 'map' in arg:
        fileid_map_file = arg
    elif 'oligos' in arg:
        oligo_file = arg
    else:
        print('Invalid file ' + arg)
        sys.exit(1)

#load huffman dictionary
print('Loading huffman for dict file: ' + dict_freq_file)
dict_freqs, ternary_huffman_dict = oligoutil.load_huffman_dictionary(dict_freq_file)

print('Loading huffman for data file: ' + data_freq_file)
data_freqs, ternary_huffman_data = oligoutil.load_huffman_dictionary(data_freq_file)

#load file id map
print('Loading fileid map from: ' + fileid_map_file)
fileid_map = oligoutil.deserialize_dictionary(fileid_map_file)

#now decode the oligo file line at a time
data_strings, dict_strings, lena_strings, mezzanine_strings = parse_oligos(oligo_file)
print('Found ' + str(len(data_strings)) + ' data strings, ' +
        str(len(dict_strings)) + ' dict strings, ' +
        str(len(lena_strings)) + ' lena strings, and ' + 
        str(len(mezzanine_strings)) + ' mezzanine strings')

decoded_data = decode_data(data_strings, ternary_huffman_data)
decoded_dict  = decode_dict(dict_strings, ternary_huffman_dict)

#flush out data files
data_files = []
path_name = os.path.dirname(sys.argv[1]) 
for key, value in decoded_data.items():
    base_name = os.path.basename(fileid_map[key])
    data_files.append(base_name[0:base_name.find('.')])
    file_name = path_name + '/' + fileid_map[key] + '.recovered'
    print('Writing decoded file: ' + file_name)
    with open(file_name, 'w') as f:
        for line in value:
            f.write(line + '\n')

#flush out dictionary
dict_file_name = path_name + '/' + '-'.join(data_files) + '.dict.recovered'
print('Writing decoded dict: ' + dict_file_name)
f = open(dict_file_name, 'wb')
f.write(decoded_dict)
f.close()

#flush out lena and mezzine if found
if (len(lena_strings)):
    lena_name = path_name + '/lena.recovered.oligos'
    print('Writing lena oligos to ' + lena_name)
    with open(lena_name, 'w') as f:
        for oligo in lena_strings:
            f.write(oligo)

if (len(mezzanine_strings)):
    mezzanine_name = path_name + '/mezzanine.recovered.oligos'
    print('Writing mezzanine oligos to ' + mezzanine_name)
    with open(mezzanine_name, 'w') as f:
        for oligo in mezzanine_strings:
            f.write(oligo)

