#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import statistics
import sys
import os.path
from collections import Counter
from collections import OrderedDict
import oligoutil
import threetup_settings

def parse_oligos(oligo_file):
    data_strings = []
    dict_strings = []
    with open(oligo_file, 'r') as f:
        for line in f:
            oligo_str = line.rstrip()
            
            #remove sense
            oligo_str = oligo_str[1:-1]

            #decode data
            encoded_data = oligoutil.decode_oligo(oligo_str)

            data_strings.append(encoded_data)

    return data_strings

def decode_data(data_strings, ternary_huffman):
    decoded_strings = []

    for encoded_str in data_strings:
	#get base3 length
        base3_str = encoded_str[-threetup_settings.G_NBASE3_LEN_CHARS:]
        length = oligoutil.todecimal(base3_str)
        encoded_str = encoded_str[: -threetup_settings.G_NBASE3_LEN_CHARS]

	#remove padding
        encoded_str = encoded_str[0 : length]
	
	#huffman decode 
        oligoutil.dprint('Huffman string: ' + encoded_str)
        decoded_str = ''.join(ternary_huffman.decode(encoded_str))
        oligoutil.dprint('Decoded str: ' + decoded_str)

        decoded_strings.append(decoded_str)

    return decoded_strings

data_freq_file = ''
dict_freq_file = ''
fileid_map_file = ''
oligo_file = ''
for arg in sys.argv[1:]:
    if 'dict.freqs' in arg:
        dict_freq_file = arg
    elif 'data.freqs' in arg:
        data_freq_file = arg
    elif 'map' in arg:
        fileid_map_file = arg
    elif 'oligos' in arg:
        oligo_file = arg
    else:
        print('Invalid file ' + arg)
        sys.exit(1)

print('Loading huffman for data file: ' + data_freq_file)
data_freqs, ternary_huffman_data = oligoutil.load_huffman_dictionary(data_freq_file)

#now decode the oligo file line at a time
data_strings = parse_oligos(oligo_file)
decoded_data = decode_data(data_strings, ternary_huffman_data)

for data in decoded_data:
    print(data)
