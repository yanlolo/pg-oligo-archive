#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import statistics
import sys
import os.path
from collections import Counter
from collections import OrderedDict
import oligoutil
import settings

if len(sys.argv) != 4:
    print('./decode <fileid map file> <data freq file> <oligofile>\n')
    sys.exit(1)

def decode_data(oligo_file, ternary_huffman):
    data_strings = []
    encoded_map = {}
    with open(oligo_file, 'r') as f:
        for line in f:
            #remove primers
            oligo_str = line.rstrip()
            assert oligo_str[:settings.G_5PRIMER_LEN] == settings.G_5_PRIMER
            assert oligo_str[-settings.G_3PRIMER_LEN:] == settings.G_3_PRIMER
            oligo_str = oligo_str[settings.G_5PRIMER_LEN :
                    -(settings.G_3PRIMER_LEN)]
            oligoutil.dprint(oligo_str)

            table_primer = oligo_str[:settings.G_TABLE_PRIMER_LEN]
            table_index = settings.G_TABLE_PRIMERS.index(table_primer)
            db_primer = oligo_str[-settings.G_DB_PRIMER_LEN:]
            db_index = settings.G_DB_PRIMERS.index(db_primer)
            oligo_str = oligo_str[settings.G_TABLE_PRIMER_LEN:
                    -settings.G_DB_PRIMER_LEN]

            #remove col primer 
            col_primer = oligo_str[-settings.G_COL_PRIMER_LEN:]
            col_index = settings.G_COL_PRIMERS.index(col_primer)
            oligo_str = oligo_str[:-settings.G_COL_PRIMER_LEN]

            #remove sense
            oligo_str = oligo_str[1:-1]

            #decode metadata (direction trit, fileid trits, colid trits, parity trit)
            metadata_len = 1 + settings.G_FILEID_CHARS + settings.G_COLID_CHARS + 1
            oligo_metadata = oligo_str[40:40 + metadata_len]
            oligoutil.dprint(oligo_metadata)
            encoded_metadata = oligoutil.decode_oligo(oligo_metadata)
            oligoutil.dprint(encoded_metadata)

            #get and remove file id
            fileid_str = encoded_metadata[1:(1 + settings.G_FILEID_CHARS)]
            oligoutil.dprint(fileid_str)
            assert len(fileid_str) == settings.G_FILEID_CHARS
            fileid = oligoutil.todecimal(fileid_str) 
            if fileid != table_index:
                print("Fileid mismatch encountered " + str(fileid) +
                        str(table_index))
                sys.exit(1)

            #get and remove col id
            colid_str = encoded_metadata[-(1 + settings.G_COLID_CHARS):-1]
            assert len(colid_str) == settings.G_COLID_CHARS
            colid = oligoutil.todecimal(colid_str) 
            if colid != col_index:
                print("Colid mismatch encountered " + str(colid) +
                        str(col_index))
                sys.exit(1)

            #decode data
            oligo_str = oligo_str[0:40] + oligo_str[40 + metadata_len: ]
            oligoutil.dprint(oligo_str)
            if (encoded_metadata[0] == '1'):
                oligo_str = oligoutil.rcomplement(oligo_str)                
                oligoutil.dprint("reverse complimenting reversed oligo:" + oligo_str)
            encoded_data = oligoutil.decode_oligo(oligo_str)
            oligoutil.dprint(encoded_data)

            # check and remove parity
            parity_str = encoded_metadata[-1:]
            oligoutil.dprint(parity_str)
            encoded_metadata = encoded_metadata[:-1]
            if oligoutil.compute_parity(encoded_data + encoded_metadata) != parity_str:
                print('Parity verification failed.')
                print('oligo:' + line.rstrip())
                print('decode data:' + encoded_data)
                print('decode metadata:' + encoded_metadata)
                print('computed parity: ' +
                        str(oligoutil.compute_parity(encoded_data + encoded_metadata)))

                print('stored parity: ' + parity_str)
                sys.exit(1)
  
            data_strings.append(encoded_data + fileid_str)

            #get base3 length
            base3_str = encoded_data[-settings.G_NBASE3_LEN_CHARS:]
            length = oligoutil.todecimal(base3_str)
            encoded_data = encoded_data[: -settings.G_NBASE3_LEN_CHARS]

            #remove padding
            encoded_data = encoded_data[0 : length]
        
            #huffman decode 
            oligoutil.dprint('Huffman string: ' + encoded_data)
            decoded_str = str(colid) + '|' + ''.join(ternary_huffman.decode(encoded_data))
            oligoutil.dprint('Decoded str: ' + decoded_str)
            uid = str(db_index) + '-' + str(table_index)
            if uid not in encoded_map:
                encoded_map[uid] = []

            encoded_map[uid].append(decoded_str)

    return encoded_map

def decode_dict(dict_strings, ternary_huffman):
    segments = {}

    for encoded_str in dict_strings:
        #get fileid
        fileid_str = encoded_str[-settings.G_FILEID_CHARS:]
        fileid = oligoutil.todecimal(fileid_str) 
        encoded_str = encoded_str[: -settings.G_FILEID_CHARS]

        #get base3 length
        base3_str = encoded_str[-settings.G_NBASE3_LEN_CHARS:]
        length = oligoutil.todecimal(base3_str)
        encoded_str = encoded_str[:-settings.G_NBASE3_LEN_CHARS]

        # get index
        index_str = encoded_str[-settings.G_MAX_INDEX_LEN:]
        chunk_idx = oligoutil.todecimal(index_str)
        encoded_str = encoded_str[:-settings.G_MAX_INDEX_LEN]

        #remove padding
        encoded_str = encoded_str[0 : length]
        
        #add string at index
        if chunk_idx in segments:
            assert segments[chunk_idx] == encoded_str

        segments[chunk_idx] = encoded_str

    #now we have all segments. merge it all and decode
    huffman_str = ''.join(segments[x] for x in sorted(segments))
    oligoutil.dprint('Length of encoded str: ' + str(len(huffman_str)))

    decoded_bytes = ternary_huffman.decode(huffman_str)

    return bytearray(int(c) for c in decoded_bytes)

data_freq_file = ''
fileid_map_file = ''
oligo_file = ''
for arg in sys.argv[1:]:
    if 'data.freqs' in arg:
        data_freq_file = arg
    elif 'map' in arg:
        fileid_map_file = arg
    elif 'oligos' in arg:
        oligo_file = arg
    else:
        print('Invalid file ' + arg)
        sys.exit(1)

#load huffman dictionary
print('Loading huffman for data file: ' + data_freq_file)
data_freqs, ternary_huffman_data = oligoutil.load_huffman_dictionary(data_freq_file)

#load file id map
print('Loading fileid map from: ' + fileid_map_file)
fileid_map = oligoutil.deserialize_dictionary(fileid_map_file)

#now decode the oligo file line at a time
decoded_data = decode_data(oligo_file, ternary_huffman_data)

#flush out data files
data_files = []
path_name = os.path.dirname(sys.argv[1]) 
for key, value in decoded_data.items():
    base_name = os.path.basename(fileid_map[key])
    data_files.append(base_name[0:base_name.find('.')])
    file_name = path_name + '/' + fileid_map[key] + '.recovered'
    print('Writing decoded file: ' + file_name)
    with open(file_name, 'w') as f:
        for line in value:
            f.write(line + '\n')
