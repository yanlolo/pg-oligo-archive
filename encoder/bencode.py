#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import statistics
import freq
import sys
import oligogen
import os.path
from collections import Counter
import huffman
import oligoutil

if len(sys.argv) < 2:
    print('./encode <filename>\n')
    sys.exit(1)

def binary_encode_data(input_name, file_id, ternary_huffman):
    print('Encoding data for file ' + input_name)
    oligos=[]

    f = open(input_name, 'rb')
    data = f.read()
    f.close()

    huffman_str = ternary_huffman.encode(data)
    print('Length of dictionary encoded str: ' + str(len(huffman_str)))

    return oligoutil.chunkify_huffman(huffman_str, file_id)

data_files = []
for arg in sys.argv[1:]:
    data_files.append(arg)

data_freqs, ternary_huffman_data = oligoutil.build_huffman_block_based(data_files)

all_oligos=[]
file_id = 0
for arg in sys.argv[1:]:
    file_id += 1
    oligos = binary_encode_data(arg, file_id, ternary_huffman_data)

    all_oligos.extend(oligos)

#print stats
for i in range(len(all_oligos)):
    print('oligo-' + str(i))
    print('\tlength: ' + str(len(all_oligos[i])))
    counter = Counter(all_oligos[i])
    print('\tGC content:' + str((counter['G'] + counter['C'])/(counter['A'] +
        counter['G'] + counter['C'] + counter['T'])))

#generate op file name
input_files = []
for arg in sys.argv[1:]:
    base_name = os.path.basename(arg)
    input_files.append(base_name[:base_name.find('.')])

oligo_file_name = os.path.dirname(sys.argv[1]) + '/' + '-'.join(input_files) + '.oligos'

#flush oligos
print('Writing oligos to file: ' + oligo_file_name);
oligo_file = open(oligo_file_name, 'w')
oligo_file.write('\n'.join(all_oligos))
oligo_file.close

#flush dictionary huffman frequency
oligoutil.serialize_dictionary(data_freqs, os.path.dirname(sys.argv[1]) + '/' + '-'.join(input_files) + '.freqs')
