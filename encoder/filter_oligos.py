#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# This script separates out the database oligos from lena and mezzanine oligos

import statistics
import sys
import os.path
from collections import Counter
from collections import OrderedDict
import oligoutil
import settings
import operator

def parse_oligos(oligo_file):
    data_oligo_strings = []
    per_file_oligo_strings = {}
    for fileid in range(0,10):
        per_file_oligo_strings[fileid] = []
    lena_strings = []
    nsense_failure = 0
    nparity_failure = 0
    ndecode_failure = 0
    nfileid_failure = 0
    noligos = 0
    nforward = 0
    nreverse = 0
    mezzanine_strings = []
    with open(oligo_file, 'r') as f:
        for line in f:
            noligos += 1
            #remove primers and sense NTs
            oligo_str = line.rstrip()
            oligo_str_cp = oligo_str
            oligoutil.dprint("Input oligo: " + oligo_str)

            # check sense
            if oligo_str[0] == 'G' or oligo_str[0] == 'C':
                if oligo_str[-1] != 'T' and oligo_str[-1] != 'A':
                    nsense_failure += 1
                    continue
                else:
                    #for now, we do only forward
                    nreverse += 1
                    continue
            elif oligo_str[0] == 'T' or oligo_str[0] == 'A':
                if oligo_str[-1] != 'G' and oligo_str[-1] != 'C':
                    nsense_failure += 1
                    continue
                
                nforward += 1

            #remove sense
            oligo_str = oligo_str[1 :-1]

            #decode metadata (direction trit, fileid trits, parity trit)
            metadata_len = 1 + settings.G_FILEID_CHARS + 1
            oligo_metadata = oligo_str[40:40 + metadata_len]
            oligoutil.dprint("Raw Metadata: " + oligo_metadata)
            try:
                encoded_metadata = oligoutil.decode_oligo(oligo_metadata)
            except:
                ndecode_failure += 1
                continue

            oligoutil.dprint("Encoded metadata: " + encoded_metadata)

            #get and remove file id
            fileid_str = encoded_metadata[1:-1]
            oligoutil.dprint("raw fileid: " + fileid_str)
            assert len(fileid_str) == settings.G_FILEID_CHARS
            fileid = oligoutil.todecimal(fileid_str) 
            if (fileid == 9):
                mezzanine_strings.append(line)
                continue

            if (fileid == 10):
                lena_strings.append(line)
                continue

            if fileid > 10:
                nfileid_failure += 1
                continue

            data_oligo_strings.append(line)
            per_file_oligo_strings[fileid].append(line)

    print("From " + str(noligos) + " oligos, found " +
            str(nsense_failure) + " sense failures, " +
            str(nfileid_failure) + " fileid failures, " +
            str(nparity_failure) + " parity failurs, " +
            str(ndecode_failure) + " decode failurs, " +
            str(nforward) + " valid forward reads, " + 
            str(nreverse) + " valid reverse reads.")

    return per_file_oligo_strings, data_oligo_strings, lena_strings, mezzanine_strings

if len(sys.argv) != 2:
    print('./filter_oligos <input.oligo>\n')
    sys.exit(1)

#now decode the oligo file line at a time
oligo_file = sys.argv[1]
per_file_oligo_strings, data_oligo_strings, lena_strings, mezzanine_strings = parse_oligos(oligo_file)
print('Found ' + str(len(data_oligo_strings)) + ' data strings, ' +
        str(len(lena_strings)) + ' lena strings, and ' + 
        str(len(mezzanine_strings)) + ' mezzanine strings')

#flush out data files
path_name = os.path.dirname(sys.argv[1]) 
file_name = os.path.basename(sys.argv[1])[:-len(".fastq")]
data_name = path_name + "/" + file_name + ".data.fastq"
lena_name = path_name + "/" + file_name + ".lena.fastq"
mezzanine_name = path_name + "/" + file_name + ".mezzanine.fastq"

with open(data_name, 'w') as f:
    for oligo in data_oligo_strings:
        f.write(oligo)

for fileid in range(0,10):
    print("Found " + str(len(per_file_oligo_strings[fileid])) + " oligos for file " + str(fileid));
    if len(per_file_oligo_strings[fileid]) > 0:
        data_name = path_name + "/" + "fileid."+ str(fileid) + "." + file_name + ".data.fastq"
        with open(data_name, 'w') as f:
            for oligo in per_file_oligo_strings[fileid]:
                f.write(oligo)

#flush out lena and mezzine
with open(lena_name, 'w') as f:
    for oligo in lena_strings:
        f.write(oligo)

with open(mezzanine_name, 'w') as f:
    for oligo in mezzanine_strings:
        f.write(oligo)

