#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Encoding format
# Data 
# ----
# | SENSE | data G_NHUFFMAN_CHARS | len G_NBASE3_LEN | SENSE |
#

import statistics
import sys
import os.path
from collections import Counter
import oligoutil
import threetup_settings

if len(sys.argv) < 2:
    print('./encode <filename>\n')
    sys.exit(1)

def huffman_to_oligo(input_str, file_id):
    huffman_len = len(input_str)
    huffman_str = input_str.ljust(threetup_settings.G_NHUFFMAN_CHARS, '0')

    # get base3 length, pad it, and append it to huffman string
    base3_str = oligoutil.tobase3(huffman_len)
    assert len(base3_str) <= threetup_settings.G_NBASE3_LEN_CHARS
    base3_str = base3_str.rjust(threetup_settings.G_NBASE3_LEN_CHARS, '0')
    encoded_str = huffman_str + base3_str    
    oligo_str = oligoutil.generate_oligo(encoded_str)
            
    return oligoutil.add_primer_sense(oligo_str, False)

def data_encode(input_name, file_id, ternary_huffman):
    oligos=[]
    line_len = []

    with open(input_name) as f:
        encoded_str = ''
        nlines = 0

        for line in f:
            oligoutil.dprint(line.rstrip())

            input_str = line.rstrip()
            huffman_str = ternary_huffman.encode(input_str)
            huffman_len = len(huffman_str)
            oligoutil.dprint('merged huffman length: ' + str(huffman_len))
            line_len.append(huffman_len)
            assert huffman_len <= threetup_settings.G_NHUFFMAN_CHARS

            oligo_str = huffman_to_oligo(huffman_str, file_id)

            oligos.append(oligo_str)

            oligoutil.dprint('oligo length: ' + str(len(oligo_str)))

    print('Done encoding file ' + input_name)
    oligoutil.dprint('Huffman code min length: ' + str(min(line_len)) + ', max: ' +
            str(max(line_len)) + ', median: ' +
            str(statistics.median(line_len)))

    return oligos

data_files = []
for arg in sys.argv[1:]:
    data_files.append(arg)

print('Building huffman DS for data files');
data_freqs, ternary_huffman_data = oligoutil.build_huffman_line_based(data_files)

all_oligos=[]
file_id = 0
fileid_map = {}
for arg in sys.argv[1:]:
    fileid_map[file_id] = os.path.basename(arg);
    print('Encoding data for file ' + arg)

    oligos = data_encode(arg, file_id, ternary_huffman_data)

    print('File ' + arg + ' generated ' + str(len(oligos)) + ' oligos.')
    all_oligos.extend(oligos)
    file_id += 1

if [oligo for oligo in all_oligos if len(oligo) != len(all_oligos[0])]:
    print('ERROR: Not all oligos same in length')
    sys.exit(1)

#add redundancy

#print stats
min_gc = 1
max_gc = 0
for i in range(len(all_oligos)):
#    print('oligo-' + str(i))
#    print('\tlength: ' + str(len(all_oligos[i])))
    counter = Counter(all_oligos[i])
    gc_content = (counter['G'] + counter['C'])/(counter['A'] +
            counter['G'] + counter['C'] + counter['T'])
    min_gc = gc_content if min_gc > gc_content else min_gc
    max_gc = gc_content if max_gc < gc_content else max_gc

#    print('\tGC content:' + str(gc_content))
    assert threetup_settings.G_GC_LOW <= gc_content <= threetup_settings.G_GC_HIGH

print("mingc: " + str(min_gc) + " maxgc: " + str(max_gc))

#generate op file name
input_files = []
for arg in sys.argv[1:]:
    base_name = os.path.basename(arg)
    input_files.append(base_name[:base_name.find('.')])

oligo_file_name = os.path.dirname(sys.argv[1]) + '/' + '-'.join(input_files) + '.oligos'

#flush oligos
print('Writing ' + str(len(all_oligos)) + ' oligos to file: ' + oligo_file_name);
oligo_file = open(oligo_file_name, 'w')
oligo_file.write('\n'.join(all_oligos))
oligo_file.close

#flush data huffman frequency
data_freq_fname = os.path.dirname(sys.argv[1]) + '/' + '-'.join(input_files) + '.data.freqs'
oligoutil.serialize_dictionary(data_freqs, data_freq_fname)
