class OligoEncoder(object):
    def __init__(self):
        self.trit_oligo_map = {
                #last-nt: next-trit-to-encode[0, 1, 2]
                'A':['C','G','T'],
                'C':['G','T','A'],
                'G':['T','A','C'],
                'T':['A','C','G']}

    def encode(self, trits):
        last_nt = 'A'
        oligo = ''
        for trit in trits:
            new_nt = self.trit_oligo_map[last_nt][int(trit)]
            last_nt = new_nt
            oligo += new_nt

        return oligo

    def decode(self, oligo):
        next_trit = self.trit_oligo_map['A'].index(oligo[0])
        decoded_str = str(next_trit)
        next_row = oligo[0]

        for nt in oligo[1:]:
            next_trit = self.trit_oligo_map[next_row].index(nt)
            decoded_str += str(next_trit)
            next_row = nt

        return decoded_str
