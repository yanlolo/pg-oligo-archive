import freq
import huffman
import settings
import oligogen
import oligoutil
import random
import pickle
import os

def dprint(message):
    if settings.G_LOG_LEVEL > 0:
        print(message)

def chunks(s, n):
    for start in range(0, len(s), n):
        yield s[start:start+n]

def tobase3(n):
    if n == 0:
        return '0'
    nums = []
    while n:
        n, r = divmod(n, 3)
        nums.append(str(r))
    return ''.join(reversed(nums))

def todecimal(n):
    val = 0
    raised = 0
    for c in reversed(n):
        val += (int(c) * pow(3, raised))
        raised += 1

    return val

def serialize_dictionary(dictionary, dict_file):
    f = open(dict_file, 'wb')
    pickle.dump(dictionary, f)
    f.close()

def deserialize_dictionary(dict_file):
    f = open(dict_file, 'rb')
    dictionary = pickle.load(f)
    f.close()

    return dictionary

def load_huffman_dictionary(dict_file):
    freq_dict = deserialize_dictionary(dict_file)
    freqs = list(freq_dict.items())
    ternary_huffman = huffman.HuffmanCode(freqs, 3)

    return freq_dict, ternary_huffman

def rcomplement(oligo):
    complement_map = {'A':'T','G':'C','T':'A','C':'G'}
    return ''.join([complement_map[x] for x in oligo[::-1]])

def build_huffman_block_based(files):
    data = bytes()
    size = 0
    for file_name in files:
        with open(file_name, 'rb') as f:
            print('Building dictionary with file ' + file_name);
            data += f.read()
            size += os.path.getsize(file_name)

    freq_dict = freq.str_freq(data, size)
    freqs = list(freq_dict.items())
    ternary_huffman = huffman.HuffmanCode(freqs, 3)

    return freq_dict, ternary_huffman

def build_huffman_line_based(files):
    data = ''
    for file_name in files:
        with open(file_name) as f:
            print('Building dictionary with file ' + file_name);

            for line in f:
                data += line.rstrip()

    freq_dict = freq.str_freq(data, len(data))
    freqs = list(freq_dict.items())
    ternary_huffman = huffman.HuffmanCode(freqs, 3)

    return freq_dict, ternary_huffman

def compute_parity(encoded_str):
    encoded_ints = [int(x) for x in list(encoded_str)]
    parity = sum(encoded_ints[1::2]) % 3
    return str(parity)

def generate_oligo(encoded_str):
    oligo_encoder = oligogen.OligoEncoder()
    oligo_string = oligo_encoder.encode(encoded_str)

    return oligo_string

def add_primer_sense(input_string, metadata_str, add_primer):
    #add sense nts
    if input_string.startswith('A'):
        prefix = 'T'
    elif input_string.startswith('T'):
        prefix = 'A'
    else:
        prefix = random.choice(['A','T'])

    if input_string.endswith('G'):
        postfix = 'C'
    elif input_string.endswith('C'):
        postfix = 'G'
    else:
        postfix = random.choice(['G','C'])

    #output_string = prefix + input_string + postfix
    dprint(prefix + '---' + input_string[0:40] + '---' + metadata_str + '---' +
            input_string[40:] + '---' + postfix)
    output_string = prefix + input_string[0:40] + metadata_str + input_string[40:] + postfix

    #add primers
    if add_primer == True:
        output_string = settings.G_5_PRIMER + output_string + settings.G_3_PRIMER

    return output_string

def add_3dprimer_sense(input_string, metadata_str, db_id, table_id, col_id, add_primer):
    #add sense nts
    if input_string.startswith('A'):
        prefix = 'T'
    elif input_string.startswith('T'):
        prefix = 'A'
    else:
        prefix = random.choice(['A','T'])

    if input_string.endswith('G'):
        postfix = 'C'
    elif input_string.endswith('C'):
        postfix = 'G'
    else:
        postfix = random.choice(['G','C'])

    #output_string = prefix + input_string + postfix
    output_string = prefix + input_string[0:40] + metadata_str + input_string[40:] + postfix

    #add primers
    print("db id is " + str(db_id))
    print("col id is " + str(col_id))
    print("tab id is " + str(table_id))
    if add_primer == True:
        dprint( settings.G_5_PRIMER + '---' +\
            settings.G_TABLE_PRIMERS[table_id] + '---' +\
            output_string + '---' +\
            prefix + '---' +\
            input_string[0:40] + '---' +\
            metadata_str + '---' +\
            input_string[40:] + '---' +\
            postfix + '---' +\
            settings.G_COL_PRIMERS[col_id] + '---' +\
            settings.G_DB_PRIMERS[db_id] + '---' +\
            settings.G_3_PRIMER )

        output_string = settings.G_5_PRIMER + \
            settings.G_TABLE_PRIMERS[table_id] + \
            output_string + \
            settings.G_COL_PRIMERS[col_id] + \
            settings.G_DB_PRIMERS[db_id] + \
            settings.G_3_PRIMER

    return output_string


def decode_oligo(oligo_str):
    oligo_encoder = oligogen.OligoEncoder()
    encoded_str = oligo_encoder.decode(oligo_str)

    return encoded_str

def chunkify_huffman(encoded_data, file_id):
    oligos = []
    rev_oligos = []

    chunks = [ encoded_data[i:i + settings.G_MAX_OLIGO_LEN] for i in range(0,
        len(encoded_data),
        settings.G_MAX_OLIGO_LEN) ]

    chunk_idx = 0
    index_length = len(tobase3(len(chunks)))
    print('Encoded dictionary with ' + str(len(chunks)) + " chunks")
    assert index_length <= settings.G_MAX_INDEX_LEN
    for chunk in chunks:
        chunk_len = len(chunk)
        encoded_str = chunk.ljust(settings.G_MAX_OLIGO_LEN, '0')

        # get index in base3 and append it
        idx_str = tobase3(chunk_idx).rjust(settings.G_MAX_INDEX_LEN, '0')
        encoded_str += idx_str

        # get base3 length and append it to huffman string
        base3_str = oligoutil.tobase3(chunk_len)
        assert len(base3_str) <= settings.G_NBASE3_LEN_CHARS
        base3_str = base3_str.rjust(settings.G_NBASE3_LEN_CHARS, '0')
        encoded_str += base3_str
        oligo_str = generate_oligo(encoded_str)
        rencoded_str = encoded_str
        roligo_str = rcomplement(oligo_str)
 
        # one trit to indicate forward mirror
        metadata_str = '0'
        encoded_str += metadata_str

        # convert file id to trits
        fileid_str = tobase3(file_id)
        assert len(fileid_str) <= settings.G_FILEID_CHARS
        fileid_str = fileid_str.rjust(settings.G_FILEID_CHARS, '0')
        metadata_str += fileid_str
        encoded_str += fileid_str

        #compute parity trit
        metadata_str += compute_parity(encoded_str)

        #covert to dna string
        metadata_oligo_str = generate_oligo(metadata_str)
        oligo_str = add_primer_sense(oligo_str, metadata_oligo_str, True)
        oligos.append(oligo_str)
        dprint('Chunk: ' + chunk)
        dprint('Chunk length: ' + str(len(chunk)))
        dprint('Encoded oligo length: ' + str(len(oligo_str)))

        #now do the same for reverse
        rmetadata_str = '1'
        rencoded_str += rmetadata_str

        rmetadata_str += fileid_str
        rencoded_str += fileid_str

        rmetadata_str += compute_parity(rencoded_str)

        rmetadata_oligo_str = generate_oligo(rmetadata_str)
        roligo_str = add_primer_sense(roligo_str, rmetadata_oligo_str, True)

        oligos.append(roligo_str)

        chunk_idx += 1

    return oligos

