#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import statistics
import freq
import sys
import oligogen
import os.path
from collections import Counter
import huffman
import oligoutil
import settings

def decode_data(oligo_file, ternary_huffman):
    oligos = []
    segments = {}
    with open(oligo_file) as f:
        for line in f:
            oligo_str = line.rstrip()

            #remote primers and sense nts
            assert oligo_str[:settings.G_5PRIMER_LEN] == settings.G_5_PRIMER
            assert oligo_str[-settings.G_3PRIMER_LEN:] == settings.G_3_PRIMER
            oligo_str = oligo_str[settings.G_5PRIMER_LEN + 1 :
                    -(settings.G_3PRIMER_LEN + 1)]
            oligoutil.dprint(oligo_str)

            #decode metadata (direction trit, fileid trits, parity trit)
            metadata_len = 1 + settings.G_FILEID_CHARS + 1
            oligo_metadata = oligo_str[-metadata_len:]
            oligoutil.dprint(oligo_metadata)
            encoded_metadata = oligoutil.decode_oligo(oligo_metadata)
            oligoutil.dprint(encoded_metadata)

            #decode data
            oligo_str = oligo_str[:-metadata_len]
            oligoutil.dprint(oligo_str)
            if (encoded_metadata[0] == '1'):
                oligo_str = oligoutil.rcomplement(oligo_str)                
                oligoutil.dprint("reverse complimenting reversed oligo:" +
                        oligo_str + '\n')
            encoded_data = oligoutil.decode_oligo(oligo_str)
            oligoutil.dprint(encoded_data)

            # check and remove parity
            parity_str = encoded_metadata[-1:]
            encoded_metadata = encoded_metadata[:-1]
            if oligoutil.compute_parity(encoded_data + encoded_metadata) != parity_str:
                print('Parity verification failed.')
                print('oligo: ' + line)
                print('decode data:' + encoded_data)
                print('decode metadata:' + encoded_metadata)
                sys.exit(1)
 
            #get fileid
            fileid_str = encoded_metadata[1:]
            assert len(fileid_str) == settings.G_FILEID_CHARS
            fileid = oligoutil.todecimal(fileid_str) 

	    #get base3 length
            base3_len_off = len(encoded_data) - settings.G_NBASE3_LEN_CHARS
            base3_str = encoded_data[base3_len_off:]
            length = oligoutil.todecimal(base3_str)
            encoded_data = encoded_data[0 : base3_len_off]

            # get index
            index_str = encoded_data[-settings.G_MAX_INDEX_LEN:]
            encoded_data = encoded_data[:-settings.G_MAX_INDEX_LEN]
            chunk_idx = oligoutil.todecimal(index_str)

	    #remove padding
            encoded_data = encoded_data[0 : length]
	
            #add string at index
            if chunk_idx in segments:
                if segments[chunk_idx] != encoded_data:
                    print(segments[chunk_idx] + ' - ' + encoded_data)

                assert segments[chunk_idx] == encoded_data

            segments[chunk_idx] = encoded_data

    #now we have all segments. merge it all and decode
    huffman_str = ''.join(segments[x] for x in sorted(segments))
    print('Length of encoded str: ' + str(len(huffman_str)))

    decoded_bytes = ternary_huffman.decode(huffman_str)

    return bytearray(int(c) for c in decoded_bytes)

if len(sys.argv) < 4:
    print('./decode <oligo-filename>.oligos <dict-filename>.freqs <outputfile>.out')
    sys.exit(1)

freq_file = ''
oligo_file = ''
output_file = ''
for arg in sys.argv[1:]:
    if '.freqs' in arg:
        freq_file = arg
    elif '.oligos' in arg:
        oligo_file = arg
    elif '.out' in arg:
        output_file = arg
    else:
        print('Invalid file ' + arg)
        sys.exit(1)

assert len(freq_file) and len(oligo_file) and len(output_file)

data_freq, ternary_huffman = oligoutil.load_huffman_dictionary(freq_file)
decoded_data = decode_data(oligo_file, ternary_huffman)
f = open(output_file, 'wb')
f.write(decoded_data)
f.close()
