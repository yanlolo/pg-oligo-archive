#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Encoding format
# Data 
# ----
# | 5" PRIMER | SENSE | data G_NHUFFMAN_CHARS | len G_NBASE3_LEN | DIR | fileid # G_FILEID_CHARS | 1 # parity | SENSE | 3" PRIMER |
#
# Dict (in oligoutil.chunkify)
# ----
# | 5" PRIMER | SENSE | data G_NHUFFMAN_CHARS | idx G_MAX_INDEX_LEN | len G_NBASE3_LEN | DIR | # fileid G_FILEID_CHARS | 1 parity | SENSE | 3" PRIMER |
#

import statistics
import sys
import os.path
from collections import Counter
import oligoutil
import settings

if len(sys.argv) < 2:
    print('./encode <filename>\n')
    sys.exit(1)

def encode_dict(input_name, file_id, ternary_huffman):
    #encode dictionary enmasse

    f = open(input_name, 'rb')
    data = f.read()
    f.close()

    huffman_str = ternary_huffman.encode(data)
    oligoutil.dprint('Length of dictionary encoded str: ' + str(len(huffman_str)))

    return oligoutil.chunkify_huffman(huffman_str, file_id)
 
def huffman_to_oligo(input_str, file_id):
    huffman_len = len(input_str)
    huffman_str = input_str.ljust(settings.G_NHUFFMAN_CHARS, '0')

    # get base3 length, pad it, and append it to huffman string
    base3_str = oligoutil.tobase3(huffman_len)
    assert len(base3_str) <= settings.G_NBASE3_LEN_CHARS
    base3_str = base3_str.rjust(settings.G_NBASE3_LEN_CHARS, '0')
    encoded_str = huffman_str + base3_str    
    rencoded_str = encoded_str
    oligo_str = oligoutil.generate_oligo(encoded_str)
    roligo_str = oligoutil.rcomplement(oligo_str)
            
    # one trit to indicate forward mirror
    metadata_str = '0'
    encoded_str += metadata_str

    # convert file id to trits
    fileid_str = oligoutil.tobase3(file_id)
    assert len(fileid_str) <= settings.G_FILEID_CHARS
    fileid_str = fileid_str.rjust(settings.G_FILEID_CHARS, '0')
    metadata_str += fileid_str
    encoded_str += fileid_str

    #compute parity trit
    metadata_str += oligoutil.compute_parity(encoded_str)
           
    #convert meatdata to oligo
    metadata_oligo_str = oligoutil.generate_oligo(metadata_str)
    oligo_str = oligoutil.add_primer_sense(oligo_str, metadata_oligo_str, True)

    #now do the same for reverse complement, but use same fileid
    rmetadata_str = '1'
    rencoded_str += rmetadata_str

    rmetadata_str += fileid_str
    rencoded_str += fileid_str

    rmetadata_str += oligoutil.compute_parity(rencoded_str)

    rmetadata_oligo_str = oligoutil.generate_oligo(rmetadata_str)
    roligo_str = oligoutil.add_primer_sense(roligo_str, rmetadata_oligo_str, True)

    return oligo_str, roligo_str

def column_set_encode(input_name, file_id, ternary_huffman):
    oligos=[]
    line_len = []

    assert 'lineitem' in input_name

    with open(input_name) as f:
        encoded_str = ''

        for line in f:
            columns = line.rstrip().split('|')
            lengths = [len(c) for c in columns]
            ncols = len(columns)
            total_length = sum(lengths)
            
            # pick columns so that half goes to first cset and half in second
            cset_len = 0
            stop_col = 0
            for l in lengths:
                cset_len += l
                stop_col += 1

                if cset_len >= total_length / 2:
                    break
            
            oligoutil.dprint('stop col: ' + str(stop_col) + ' total len: ' +
                    str(total_length) + ' cset_len: ' + str(cset_len))
            first_cset = '|'.join(columns[:stop_col])
            second_cset = columns[0] + '|' + columns[3] + '|' + '|'.join(columns[stop_col:])

            first_huffman_str = ternary_huffman.encode(first_cset.rstrip())
            first_huffman_len = len(first_huffman_str)
            assert first_huffman_len <= settings.G_NHUFFMAN_CHARS
            line_len.append(first_huffman_len)
            oligo_string, reverse_oligo = huffman_to_oligo(first_huffman_str,
                    file_id)

            oligos.append(oligo_string)
            oligos.append(reverse_oligo)

            second_huffman_str = ternary_huffman.encode(second_cset.rstrip())
            second_huffman_len = len(second_huffman_str)
            assert second_huffman_len <= settings.G_NHUFFMAN_CHARS
            line_len.append(second_huffman_len)
            oligo_string, reverse_oligo = huffman_to_oligo(second_huffman_str,
                    file_id)

            oligos.append(oligo_string)
            oligos.append(reverse_oligo)

            oligoutil.dprint('base line: ' + line.rstrip())
            oligoutil.dprint('first cset: ' + first_cset + ' len ' + str(len(first_cset)))
            oligoutil.dprint('first huffman line: ' + first_huffman_str)
            oligoutil.dprint('second cset: ' + second_cset + ' len ' + str(len(second_cset)))
            oligoutil.dprint('second huffman line: ' + second_huffman_str)
            oligoutil.dprint('Encoded oligo length: ' + str(len(oligo_string)))

    print('Done encoding file ' + input_name)
    oligoutil.dprint('Huffman code min length: ' + str(min(line_len)) + ', max: ' +
            str(max(line_len)) + ', median: ' +
            str(statistics.median(line_len)))

    return oligos

def range_partition_encode(input_name, file_id, ternary_huffman):
    oligos=[]
    line_len = []

    with open(input_name) as f:
        encoded_str = ''
        lines = []
        total_huffman_len = 0
        nlines = 0

        for line in f:
            huffman_str = ternary_huffman.encode(line.rstrip())

            #if adding this string would make us overflow, flush out
            if total_huffman_len + len(huffman_str) >= settings.G_NHUFFMAN_CHARS:
                oligoutil.dprint('Converting next ' + str(nlines) + ' line into an oligo')
                
                assert lines
                for l in lines:
                    oligoutil.dprint(l.rstrip())

                merged_input_str = ''.join(lines)
                merged_huffman_str = ternary_huffman.encode(merged_input_str)
                merged_huffman_len = len(merged_huffman_str)
                oligoutil.dprint('merged huffman length: ' + str(merged_huffman_len))
                line_len.append(merged_huffman_len)
                assert merged_huffman_len <= settings.G_NHUFFMAN_CHARS

                merged_oligo_str, reverse_oligo = huffman_to_oligo(merged_huffman_str,
                        file_id)

                oligos.append(merged_oligo_str)
                oligos.append(reverse_oligo)

                oligoutil.dprint('merged oligo length: ' + str(len(merged_oligo_str)))

                total_huffman_len = 0
                nlines = 0
                lines.clear()

            total_huffman_len += len(huffman_str)
            lines.append(line.rstrip())
            nlines += 1

        #encode any left overs
        if (nlines > 0) :
            assert total_huffman_len
            oligoutil.dprint('Converting next ' + str(nlines) + ' lines together into an oligo')

            for l in lines:
                oligoutil.dprint(l.rstrip())

            merged_input_str = ''.join(lines)
            merged_huffman_str = ternary_huffman.encode(merged_input_str)
            merged_huffman_len = len(merged_huffman_str)
            line_len.append(merged_huffman_len)
            oligoutil.dprint('merged huffman length: ' + str(merged_huffman_len))
            assert merged_huffman_len <= settings.G_NHUFFMAN_CHARS

            merged_oligo_str, reverse_oligo = huffman_to_oligo(merged_huffman_str,
                    file_id)

            oligos.append(merged_oligo_str)
            oligos.append(reverse_oligo)
            oligoutil.dprint('merged oligo length: ' + str(len(merged_oligo_str)))

    print('Done encoding file ' + input_name)
    oligoutil.dprint('Huffman code min length: ' + str(min(line_len)) + ', max: ' +
            str(max(line_len)) + ', median: ' +
            str(statistics.median(line_len)))

    return oligos

data_files = []
dict_file = []
for arg in sys.argv[1:]:
    if 'dict' in arg:
        dict_file.append(arg)
    else:
        data_files.append(arg)

if dict_file:
    print('Building huffman DS for dictionary');
    dict_freqs, ternary_huffman_dict = oligoutil.build_huffman_block_based(dict_file)

print('Building huffman DS for data files');
data_freqs, ternary_huffman_data = oligoutil.build_huffman_line_based(data_files)

all_oligos=[]
file_id = 0
fileid_map = {}
for arg in sys.argv[1:]:
    fileid_map[file_id] = os.path.basename(arg);
    print('Encoding data for file ' + arg)

    if 'dict' in arg:
        oligos = encode_dict(arg, file_id, ternary_huffman_dict)
    elif 'lineitem' in arg:
        oligos = column_set_encode(arg, file_id, ternary_huffman_data)
    else:
        oligos = range_partition_encode(arg, file_id, ternary_huffman_data)

    print('File ' + arg + ' generated ' + str(len(oligos)) + ' oligos.')
    all_oligos.extend(oligos)
    file_id += 1

if [oligo for oligo in all_oligos if len(oligo) != len(all_oligos[0])]:
    print('ERROR: Not all oligos same in length')
    sys.exit(1)

#add redundancy

#print stats
min_gc = 1
max_gc = 0
for i in range(len(all_oligos)):
#    print('oligo-' + str(i))
#    print('\tlength: ' + str(len(all_oligos[i])))
    counter = Counter(all_oligos[i])
    gc_content = (counter['G'] + counter['C'])/(counter['A'] +
            counter['G'] + counter['C'] + counter['T'])
    min_gc = gc_content if min_gc > gc_content else min_gc
    max_gc = gc_content if max_gc < gc_content else max_gc

#    print('\tGC content:' + str(gc_content))
    assert settings.G_GC_LOW <= gc_content <= settings.G_GC_HIGH

print("mingc: " + str(min_gc) + " maxgc: " + str(max_gc))

#generate op file name
input_files = []
for arg in sys.argv[1:]:
    if 'dict' in arg:
        continue
    base_name = os.path.basename(arg)
    input_files.append(base_name[:base_name.find('.')])

oligo_file_name = os.path.dirname(sys.argv[1]) + '/' + '-'.join(input_files) + '.oligos'

#flush oligos
print('Writing ' + str(len(all_oligos)) + ' oligos to file: ' + oligo_file_name);
oligo_file = open(oligo_file_name, 'w')
oligo_file.write('\n'.join(all_oligos))
oligo_file.write('\n')
oligo_file.close()

#flush fileid_map
map_file_name = os.path.dirname(sys.argv[1]) + '/' + '-'.join(input_files) + '.map'
oligoutil.serialize_dictionary(fileid_map, map_file_name)

#flush dictionary huffman frequency
if dict_file:
    oligoutil.serialize_dictionary(dict_freqs, dict_file[0] + '.freqs')

#flush data huffman frequency
data_freq_fname = os.path.dirname(sys.argv[1]) + '/' + '-'.join(input_files) + '.data.freqs'
oligoutil.serialize_dictionary(data_freqs, data_freq_fname)
