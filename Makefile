#INPUT_DIR=data/ssb-sf-0.0001
#INPUT_DIR=data/tpch-sf-0.0001
#INPUT_DIR=data/synthetic-database
#INPUT_DIR=data/sf-threetuples
INPUT_DIR=data/3dindex_larger_input/
BINARY_INPUT = data/lena.jpg
BINARY_OUTPUT = data/lena.recovered.out.jpg
LOG_FILE = debug.log

all: reformat

reformat:
	scripts/reformat.py $(wildcard ${INPUT_DIR}/*.tbl)

ttencode:
	encoder/threetup_encoder.py $(wildcard ${INPUT_DIR}/*.base) #> ${LOG_FILE}

ttdecode:
	encoder/threetup_decode.py $(wildcard ${INPUT_DIR}/*.freqs ${INPUT_DIR}/*.oligos)

encode:
	encoder/encode.py $(wildcard ${INPUT_DIR}/*.base ${INPUT_DIR}/*.dict) > ${LOG_FILE}

3dencode:
	encoder/3dindex_encoder.py $(wildcard ${INPUT_DIR}/*.base) > ${LOG_FILE}

bencode:
	encoder/bencode.py ${BINARY_INPUT}

bdecode:
	encoder/bdecode.py $(wildcard data/*.freqs data/*.oligos) ${BINARY_OUTPUT}

decode:
	encoder/decode.py $(wildcard ${INPUT_DIR}/*.freqs ${INPUT_DIR}/*.map ${INPUT_DIR}/*.oligos) > ${LOG_FILE}

3ddecode:
	encoder/3dindex_decoder.py $(wildcard ${INPUT_DIR}/*.freqs ${INPUT_DIR}/*.map ${INPUT_DIR}/*.oligos) > ${LOG_FILE}

clean:
	rm ${INPUT_DIR}/*.dict ${INPUT_DIR}/*.base ${INPUT_DIR}/*.oligos ${INPUT_DIR}/*.map ${INPUT_DIR}/*.freqs ${INPUT_DIR}/*.recovered data/*.oligos data/*.recovered.out.jpg
